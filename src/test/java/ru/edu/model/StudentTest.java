package ru.edu.model;

import junit.framework.TestCase;
import org.mockito.Spy;

import java.time.LocalDate;
import java.util.UUID;

public class StudentTest extends TestCase {

    UUID uuid = UUID.randomUUID();

    @Spy
    Student student = Student.builder()
            .id(uuid)
            .firstName("Goldie39")
            .lastName("Roger39")
            .birthDate(LocalDate.of(1992,6,21))
            .isGraduated(true)
            .build();

    public void testGraduatedInt() {
        assertEquals(1,Student.graduatedInt(student.isGraduated()));
    }

    public void testGraduatedToBoolean() {
        assertTrue(Student.graduatedToBoolean(1));
    }

    public void testGetId() {
        assertEquals(uuid,student.getId());
    }

    public void testGetFirstName() {
        assertEquals("Goldie39",student.getFirstName());
    }

    public void testGetLastName() {
        assertEquals("Roger39",student.getLastName());
    }

    public void testGetBirthDate() {
        assertEquals(LocalDate.of(1992,6,21),student.getBirthDate());
    }

    public void testIsGraduated() {
        assertTrue(student.isGraduated());
    }

    public void testSetId() {
        UUID newUUID = UUID.randomUUID();
        student.setId(newUUID);

        assertEquals(newUUID,student.getId());
    }

    public void testSetFirstName() {
        student.setFirstName("Shanks");
        assertEquals("Shanks",student.getFirstName());
    }

    public void testSetLastName() {
        student.setLastName("RedHat");
        assertEquals("RedHat",student.getLastName());
    }

    public void testSetBirthDate() {
        student.setBirthDate(LocalDate.now());
        assertEquals(LocalDate.now(),student.getBirthDate());
    }

    public void testSetGraduated() {
        student.setGraduated(false);
        assertFalse(student.isGraduated());
    }

    public void testTestToString() {
        String expected = student.toString();
        assertEquals(expected,student.toString());
    }

    public void testBuilder() {
        Student expected = Student.builder()
                .id(student.getId())
                .firstName(student.getFirstName())
                .lastName(student.getLastName())
                .birthDate(student.getBirthDate())
                .isGraduated(student.isGraduated())
                .build();

        assertNotNull(expected);
        assertEquals(expected.toString(),student.toString());
    }
}