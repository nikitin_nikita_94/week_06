package ru.edu;

import junit.framework.TestCase;
import lombok.SneakyThrows;
import org.junit.Test;
import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class StudentsRepositoryCRUDImplTest extends TestCase {

    String jdbcUrl = "jdbc:sqlite:chinook.db";

    StudentsRepositoryCRUDImpl crudStudent;

    List<Student> studentList = createStudents();

    private List<Student> createStudents() {
        List<Student> list = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            Student student = Student.builder()
                    .id(null)
                    .firstName("Goldie" + i)
                    .lastName("Roger" + i)
                    .birthDate(LocalDate.of(1990 + i, i, i + i))
                    .isGraduated(i >= 3)
                    .build();
            list.add(student);
        }
        return list;
    }

    @SneakyThrows
    public Connection createConnection() {
        return DriverManager.getConnection(jdbcUrl);
    }

    @Test
    public void testCreateTable() throws SQLException {
        crudStudent = new StudentsRepositoryCRUDImpl(createConnection());
        int actual = crudStudent.createTable();
        assertEquals(0, actual);
    }

    @Test(expected = RuntimeException.class)
    public void testCreateTableException() {
        crudStudent = new StudentsRepositoryCRUDImpl(createConnection());
        try {
            crudStudent.createTable();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testDropTable() {
        crudStudent = new StudentsRepositoryCRUDImpl(createConnection());
        crudStudent.dropTable();
    }

    @Test(expected = RuntimeException.class)
    public void testDropTableException() {
        crudStudent = new StudentsRepositoryCRUDImpl(createConnection());
        try {
            crudStudent.dropTable();
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        } finally {
            crudStudent.createTable();
        }
    }

    @Test
    public void testCreate() {
        crudStudent = new StudentsRepositoryCRUDImpl(createConnection());
        crudStudent.createTable();

        UUID uuid = crudStudent.create(studentList.get(0));
        UUID uuid1 = crudStudent.create(studentList.get(1));
        UUID uuid3 = crudStudent.create(studentList.get(2));
        assertNotNull(uuid);
        assertNotNull(uuid1);
        assertNotNull(uuid3);
    }

    @Test(expected = RuntimeException.class)
    public void testCreateException() {
        crudStudent = new StudentsRepositoryCRUDImpl(createConnection());
        try {
            Student student = crudStudent.selectAll().get(0);
            crudStudent.create(student);
            crudStudent.create(student);
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testSelectById() {
        crudStudent = new StudentsRepositoryCRUDImpl(createConnection());
        crudStudent.create(studentList.get(1));

        UUID uuidFirstStudent = crudStudent.selectAll().get(1).getId();
        Student actual = crudStudent.selectById(uuidFirstStudent);
        assertEquals(studentList.get(1).toString(), new Student(null, actual.getFirstName(), actual.getLastName(), actual.getBirthDate(), actual.isGraduated()).toString());

    }

    @Test(expected = RuntimeException.class)
    public void testSelectByIdException() {
        crudStudent = new StudentsRepositoryCRUDImpl(createConnection());
        try {
            crudStudent.create(studentList.get(1));

            UUID uuidFirstStudent = crudStudent.selectAll().get(1).getId();
            Student actual = crudStudent.selectById(uuidFirstStudent);
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        } finally {
            crudStudent.dropTable();
        }
    }

    @Test
    public void testSelectAll() {
        crudStudent = new StudentsRepositoryCRUDImpl(createConnection());

        crudStudent.create(studentList.get(0));
        crudStudent.create(studentList.get(1));
        crudStudent.create(studentList.get(2));

        List<Student> actualList = crudStudent.selectAll();
        assertEquals(studentList.get(0).getFirstName(), actualList.get(0).getFirstName());
        assertEquals(studentList.get(1).getFirstName(), actualList.get(1).getFirstName());
        assertEquals(studentList.get(2).getFirstName(), actualList.get(2).getFirstName());
    }

    @Test(expected = RuntimeException.class)
    public void testSelectAllException() {
        try {
            crudStudent = new StudentsRepositoryCRUDImpl(createConnection());
            crudStudent.create(studentList.get(0));
            crudStudent.create(studentList.get(1));
            crudStudent.create(studentList.get(2));
        }catch (RuntimeException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testUpdate() {
        crudStudent = new StudentsRepositoryCRUDImpl(createConnection());
        crudStudent.create(studentList.get(0));

        Student student = crudStudent.selectAll().get(0);
        student.setBirthDate(LocalDate.now());

        int result = crudStudent.update(student);
        assertEquals(1, result);
    }

    @Test(expected = RuntimeException.class)
    public void testUpdateException() {
        try {
            crudStudent = new StudentsRepositoryCRUDImpl(createConnection());
            crudStudent.create(studentList.get(0));

            Student student = crudStudent.selectAll().get(0);
            student.setBirthDate(LocalDate.now());

            crudStudent.update(student);
            crudStudent.update(student);
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        }
    }

    @Test
    public void testRemove() {
        crudStudent = new StudentsRepositoryCRUDImpl(createConnection());
        List<Student> studentList = crudStudent.selectAll();
        List<UUID> studentUuid = studentList.stream()
                .map(Student::getId)
                .collect(Collectors.toList());
        studentUuid.forEach(System.out::println);

        int actual = crudStudent.remove(studentUuid);
        assertEquals(3, actual);
    }

    @Test(expected = RuntimeException.class)
    public void testRemoveException() {
        try {
            crudStudent = new StudentsRepositoryCRUDImpl(createConnection());
            List<Student> studentList = crudStudent.selectAll();
            List<UUID> studentUuid = studentList.stream()
                    .map(Student::getId)
                    .collect(Collectors.toList());
            studentUuid.forEach(System.out::println);

            crudStudent.remove(studentUuid);
            crudStudent.remove(studentUuid);
        } catch (RuntimeException ex) {
            ex.printStackTrace();
        }
    }
}