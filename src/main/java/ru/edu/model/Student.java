package ru.edu.model;

import lombok.Getter;
import lombok.Setter;
import lombok.Builder;
import lombok.ToString;
import lombok.AllArgsConstructor;

import java.time.LocalDate;
import java.util.UUID;




@Getter
@Setter
@Builder
@ToString
@AllArgsConstructor
public class Student {

    /**
     * Первичный ключ.
     * <p>
     * Рекомендуется генерировать его только
     * внутри StudentsRepositoryCRUD.create(),
     * иными словами до момента пока объект
     * не будет сохранен в БД, он не должен
     * иметь значение id.
     */
    private UUID id;

    /**
     * Имя студента.
     */
    private String firstName;

    /**
     * Фамилия студента.
     */
    private String lastName;

    /**
     * День рождение студента.
     */
    private LocalDate birthDate;

    /**
     * Закончил ли обучение данный студент.
     */
    private boolean isGraduated;

    /**
     * Метод для перевода boolean
     * значения в int.
     * @param isGraduated
     * @return int
     */
    public static int graduatedInt(final boolean isGraduated) {
        return !isGraduated ? 0 : 1;
    }

    /**
     * Перевод int значения в boolean.
     * @param value
     * @return boolean
     */
    public static boolean graduatedToBoolean(final int value) {
        return value > 0;
    }
}
