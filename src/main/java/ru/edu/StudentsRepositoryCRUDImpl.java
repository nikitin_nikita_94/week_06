package ru.edu;

import lombok.AllArgsConstructor;
import ru.edu.model.Student;

import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@AllArgsConstructor
public class StudentsRepositoryCRUDImpl implements StudentsRepositoryCRUD {

    /**
     * Magic number.
     */
    private static final int ONE = 1;

    /**
     * Magic number.
     */
    private static final int TWO = 2;

    /**
     * Magic number.
     */
    private static final int THREE = 3;

    /**
     * Magic number.
     */
    private static final int FORE = 4;

    /**
     * Magic number.
     */
    private static final int FIVE = 5;

    /**
     * Позволяет создать соединение —
     * возвращает экземпляр класса.
     */
    private Connection connection;

    /**
     * Создание таблицы в базе данных.
     *
     * @return int
     */
    public int createTable() {
        try (Statement statement = connection.createStatement()) {
            String sql = "create table students ("
                    + "id varchar(50),"
                    + "firstName varchar(32),"
                    + "lastName varchar(32),"
                    + "birthDate text,"
                    + "isGraduated integer(1)"
                    + ");";
            int affectiveRows = statement.executeUpdate(sql);
            System.out.println("create table affectiveRows " + affectiveRows);
            return affectiveRows;
        } catch (Exception ex) {
            throw new RuntimeException("Error creating the table: "
                    + ex.toString(), ex);
        }
    }

    /**
     * Создание записи в БД.
     * id у student должен быть null, иначе требуется вызов update.
     * id генерируем через UUID.randomUUID()
     *
     * @param student
     * @return сгенерированный UUID
     */
    @Override
    public UUID create(final Student student) {
        if (student.getId() != null) {
            update(student);
        }

        try (Statement statement = connection.createStatement()) {
            UUID uuid = UUID.randomUUID();

            String sql = String.format("insert into students "
                            + "('id', 'firstName','lastName',"
                            + " 'birthDate','isGraduated') "
                            + "VALUES('%s','%s','%s',date('%s'),%d)",
                    uuid, student.getFirstName(), student.getLastName(),
                    student.getBirthDate(),
                    Student.graduatedInt(student.isGraduated()));

            boolean hasResultSet = statement.execute(sql);
            System.out.println("Insert students Table complete"
                    + " hasResultSet " + hasResultSet);

            return uuid;
        } catch (Exception ex) {
            throw new RuntimeException("Error Incorrect input students= "
                    + student.toString(), ex);
        }
    }

    /**
     * Удаление таблицы в базе данных.
     */
    public void dropTable() {
        try (Statement statement = connection.createStatement();) {
            int affectiveRows = statement.executeUpdate("drop table students");
            System.out.println("drop table affectiveRows " + affectiveRows);
        } catch (Exception ex) {
            throw new RuntimeException("Error drop table students :"
                    + ex.toString(), ex);
        }
    }


    /**
     * Получение записи по id из БД.
     *
     * @param id
     * @return
     */
    @Override
    public Student selectById(final UUID id) {
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(
                     "select * from students where id = '" + id + "'")) {

            if (!resultSet.next()) {
                return null;
            }

            UUID studentId = UUID.fromString(resultSet.getString("id"));
            String firstName = resultSet
                    .getString("firstName");
            String lastName = resultSet
                    .getString("lastName");
            LocalDate birthDate = LocalDate
                    .parse(resultSet.getString("birthDate"));
            boolean isGraduated = resultSet
                    .getBoolean("isGraduated");

            System.out.println("selectById complected id " + id);
            return new Student(studentId, firstName, lastName,
                    birthDate, isGraduated);
        } catch (Exception ex) {
            throw new RuntimeException("Failed to selectById"
                    + "error= " + ex.toString(), ex);
        }
    }

    /**
     * Получение всех записей из БД.
     *
     * @return
     */
    @Override
    public List<Student> selectAll() {
        try (Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(
                     "select * from students")) {
            List<Student> list = new ArrayList<>();

            while (resultSet.next()) {
                UUID studentId = UUID.fromString(resultSet
                        .getString("id"));
                String firstName = resultSet
                        .getString("firstName");
                String lastName = resultSet
                        .getString("lastName");
                LocalDate birthDate = LocalDate.parse(resultSet
                        .getString("birthDate"));
                boolean isGraduated = resultSet
                        .getBoolean("isGraduated");
                list.add(new Student(studentId, firstName, lastName,
                        birthDate, isGraduated));
            }

            return list;
        } catch (Exception ex) {
            throw new RuntimeException("Error method selectAll crush : "
                    + ex.toString(), ex);
        }
    }

    /**
     * Обновление записи в БД.
     *
     * @param student
     * @return количество обновленных записей
     */
    @Override
    public int update(final Student student) {
        String sql = "update students set "
                + "firstName = ? ,"
                + "lastName = ? ,"
                + "birthDate = ? ,"
                + "isGraduated = ?"
                + "where id = ?";

        try (PreparedStatement pstmt = connection.prepareStatement(sql)) {
            pstmt.setString(ONE, student.getFirstName());

            pstmt.setString(TWO, student.getLastName());

            pstmt.setString(THREE, String.valueOf(student.getBirthDate()));

            pstmt.setBoolean(FORE, student.isGraduated());

            pstmt.setString(FIVE, String.valueOf(student.getId()));

            return pstmt.executeUpdate();
        } catch (Exception ex) {
            throw new RuntimeException("Error update is fail :"
                    + ex.toString(), ex);
        }
    }

    /**
     * Удаление указанных записей по id.
     *
     * @param idList
     * @return количество удаленных записей
     */
    @Override
    public int remove(final List<UUID> idList) {
        return idList.stream()
                .mapToInt(this::auxiliaryMethodForRemovingOneUuid)
                .sum();
    }

    /**
     * Вспомогательный метод для
     * удаления одного студента.
     *
     * @param uuid
     * @return int
     */
    private int auxiliaryMethodForRemovingOneUuid(final UUID uuid) {
        try (PreparedStatement statement = connection.prepareStatement(
                "delete from students where id = ?")) {
            statement.setString(1, String.valueOf(uuid));

            return statement.executeUpdate();
        } catch (Exception ex) {
            throw new RuntimeException("Failed to remove" + "error= "
                    + ex.toString(), ex);
        }
    }
}
